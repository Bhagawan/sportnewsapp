package com.example.sportnews.data

import androidx.annotation.Keep
import com.tickaroo.tikxml.annotation.Element
import com.tickaroo.tikxml.annotation.Xml

@Xml
@Keep
data class Rss(@Element(name = "channel") val channel: RssChannel)