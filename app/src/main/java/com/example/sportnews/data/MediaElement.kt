package com.example.sportnews.data

import androidx.annotation.Keep
import com.tickaroo.tikxml.annotation.Attribute
import com.tickaroo.tikxml.annotation.Xml

@Keep
@Xml
data class MediaElement(@Attribute(name = "url") val url: String)