package com.example.sportnews.data

import androidx.annotation.Keep

@Keep
data class SportNewsSplashResponse(val url : String)