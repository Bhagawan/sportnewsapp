package com.example.sportnews.data

import androidx.annotation.Keep
import com.tickaroo.tikxml.annotation.Element
import com.tickaroo.tikxml.annotation.PropertyElement
import com.tickaroo.tikxml.annotation.Xml

@Keep
@Xml
data class NewsPiece (@PropertyElement(name = "title") val title: String,
    @PropertyElement(name = "link") val link: String,
    @PropertyElement(name = "description") val description: String,
    @Element(name = "media:content") val icon: MediaElement?)