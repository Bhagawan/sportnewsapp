package com.example.sportnews.data

import androidx.annotation.Keep
import com.tickaroo.tikxml.annotation.Element
import com.tickaroo.tikxml.annotation.Xml

@Keep
@Xml
data class RssChannel(@Element(name = "item") val news: List<NewsPiece>)
