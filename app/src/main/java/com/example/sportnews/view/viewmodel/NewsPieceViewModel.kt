package com.example.sportnews.view.viewmodel

import com.example.sportnews.data.NewsPiece

class NewsPieceViewModel(val newsPiece: NewsPiece) {

    val desc = newsPiece.description.subSequence(11, newsPiece.description.length - 12)
    val img = newsPiece.icon?.url ?: ""

}