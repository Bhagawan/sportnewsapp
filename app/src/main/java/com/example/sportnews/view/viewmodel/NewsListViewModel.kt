package com.example.sportnews.view.viewmodel

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.sportnews.BR
import com.example.sportnews.data.NewsPiece
import com.example.sportnews.util.SportNewsServerClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableSharedFlow

class NewsListViewModel(val address: String): BaseObservable() {

    var error = MutableSharedFlow<String>()
    private val request: Job?

    @Bindable
    var news: List<NewsPiece>? = null

    init {
        request = CoroutineScope(Dispatchers.IO).async {
            getRss()
        }
    }

    private suspend fun getRss() {
        try{
            val response = SportNewsServerClient.create().getNews(address)
            if(response.isSuccessful) {
                news = response.body()?.channel?.news
                notifyPropertyChanged(BR.news)
            }
        } catch (e: Exception) {
            error.emit(e.message ?: "server error")
        }
    }

}