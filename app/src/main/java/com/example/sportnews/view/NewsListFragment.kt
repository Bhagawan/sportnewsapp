package com.example.sportnews.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.library.baseAdapters.BR
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.example.sportnews.R
import com.example.sportnews.databinding.FragmentNewsListBinding
import com.example.sportnews.databinding.ItemNewsBinding
import com.example.sportnews.util.adapters.NewsListAdapter
import com.example.sportnews.view.viewmodel.NewsItemViewModel
import com.example.sportnews.view.viewmodel.NewsListViewModel
import kotlinx.coroutines.launch

class NewsListFragment(url: String) : Fragment() {
    private lateinit var binding: FragmentNewsListBinding
    private val viewModel = NewsListViewModel(url)
    private var popupWindow: PopupWindow? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentNewsListBinding.inflate(layoutInflater)
        binding.viewmodel = viewModel
        binding.recyclerNews.adapter = NewsListAdapter().apply { setInterface(object : NewsListAdapter.NewsAdapterCallback {
            override fun onClick(url: String) {
                showNews(url)
            }
        }) }

        lifecycleScope.launch { viewModel.error.collect {
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
        } }

        return binding.root
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun showNews(url: String) {
        val popupBinding: ItemNewsBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.item_news, null, false)

        val width = LinearLayout.LayoutParams.MATCH_PARENT
        val height = LinearLayout.LayoutParams.MATCH_PARENT

        val popupWindow = PopupWindow(popupBinding.root, width, height, true)
        val popupViewModel = NewsItemViewModel(url)

        popupWindow.setBackgroundDrawable( ColorDrawable(Color.TRANSPARENT))
        popupWindow.isTouchable = true
        popupWindow.isOutsideTouchable = true

        popupBinding.itemDim.setOnClickListener { popupWindow.dismiss() }

        popupWindow.setTouchInterceptor{ _, event ->
            if (event?.action == MotionEvent.ACTION_DOWN) {
                val v = popupBinding.root.findFocus()
                if (v is EditText) {
                    val outRect = Rect()
                    v.getGlobalVisibleRect(outRect)
                    if (!outRect.contains(event.rawX.toInt(), event.rawY.toInt())) {
                        v.clearFocus()
                        val imm = context?.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                    }
                }
            }
            false
        }

        popupBinding.setVariable(BR.viewmodel, popupViewModel)

        popupWindow.animationStyle = R.style.NewsAnimation
        popupWindow.showAtLocation(binding.root, Gravity.CENTER, 0,0)

        val p = popupWindow.contentView.rootView.layoutParams as WindowManager.LayoutParams
        p.flags = p.flags or WindowManager.LayoutParams.FLAG_DIM_BEHIND or WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        p.dimAmount = 0.26f

        val wm: WindowManager = context?.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        wm.updateViewLayout(popupWindow.contentView.rootView, p)
        this.popupWindow = popupWindow
    }


}