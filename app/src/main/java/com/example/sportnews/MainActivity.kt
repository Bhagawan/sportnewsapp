package com.example.sportnews

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.sportnews.databinding.ActivityMainBinding
import com.example.sportnews.util.adapters.PagerAdapter
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var callback: ViewPager2.OnPageChangeCallback
    private lateinit var target: Target

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        binding.viewPager.adapter = PagerAdapter(this)
        callback = object :ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                binding.bottomNavigationView.selectedItemId = when(position) {
                    0 -> R.id.menu_summer
                    1 -> R.id.menu_winter
                    else -> R.id.menu_other
                }
            }
        }
        binding.viewPager.registerOnPageChangeCallback(callback)
        binding.bottomNavigationView.setOnItemSelectedListener {
            binding.viewPager.setCurrentItem(when(it.itemId) {
                R.id.menu_summer -> 0
                R.id.menu_winter -> 1
                else -> 2
            }, true)
            true
        }
        setContentView(binding.root)
        loadBackground()
    }

    private fun loadBackground() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let { binding.root.background = BitmapDrawable(resources, it) }
            }

            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) { }

            override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }

        }
        Picasso.get().load("https://www.sport.ru/ai/34x16000/531526/head_0.jpg").into(target)
    }

    override fun onDestroy() {
        binding.viewPager.unregisterOnPageChangeCallback(callback)
        super.onDestroy()
    }
}