package com.example.sportnews.util

import com.example.sportnews.data.Rss
import com.example.sportnews.data.SportNewsSplashResponse
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.http.*

interface SportNewsServerClient {

    @FormUrlEncoded
    @POST("SportNewsApp/splash.php")
    suspend fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String): Response<SportNewsSplashResponse>

    @GET @AsXml
    suspend fun getNews(@Url url: String) : Response<Rss>

    companion object {
        fun create() : SportNewsServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(XmlOrJsonConverterFactory())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(SportNewsServerClient::class.java)
        }
    }

}