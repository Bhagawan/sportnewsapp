package com.example.sportnews.util.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.sportnews.view.NewsListFragment

class PagerAdapter(activity: FragmentActivity) : FragmentStateAdapter(activity) {
    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment = when(position) {
        0 -> NewsListFragment("https://www.sport.ru/rssfeeds/news.rss")
        1 -> NewsListFragment("https://www.sport.ru/rssfeeds/winter.rss")
        2 -> NewsListFragment("https://www.sport.ru/rssfeeds/other.rss")
        else -> { NewsListFragment("https://www.sport.ru/rssfeeds/other.rss") }
    }
}