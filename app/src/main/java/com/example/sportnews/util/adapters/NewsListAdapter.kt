package com.example.sportnews.util.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.sportnews.BR
import com.example.sportnews.R
import com.example.sportnews.data.NewsPiece
import com.example.sportnews.util.DynamicAdapter
import com.example.sportnews.view.viewmodel.NewsPieceViewModel

class NewsListAdapter : RecyclerView.Adapter<NewsListAdapter.ViewHolder>(), DynamicAdapter<List<NewsPiece>> {
    private var items = emptyList<NewsPiece>()
    private var i: NewsAdapterCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ViewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_news_preview, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
        holder.itemView.setOnClickListener { i?.onClick(items[position].link) }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun setData(data: List<NewsPiece>) {
        items = data
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: NewsPiece) {
            val viewModel = NewsPieceViewModel(item)
            binding.setVariable(BR.viewmodel, viewModel)
        }
    }

    fun setInterface(i: NewsAdapterCallback) {
        this.i = i
    }

    interface NewsAdapterCallback {
        fun onClick(url: String)
    }
}