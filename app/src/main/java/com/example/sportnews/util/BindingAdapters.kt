package com.example.sportnews.util

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import im.delight.android.webview.AdvancedWebView

@Suppress("UNCHECKED_CAST")
@BindingAdapter("recyclerData")
fun <T> setData(recycler: RecyclerView, data: T?) {
    if(recycler.adapter != null && data != null){
        if(recycler.adapter is DynamicAdapter<*>) (recycler.adapter as DynamicAdapter<T>).setData(data)
    }
}

@BindingAdapter("imageUrl")
fun loadUrl(view: ImageView, url: String) {
    if(url.isNotEmpty()) Picasso.get().load(url).into(view)
}

@BindingAdapter("url")
fun setUrl(v: AdvancedWebView, url: String) {
    v.loadUrl(url)
}
