package com.example.sportnews.util

interface DynamicAdapter<T> {
    fun setData(data: T)
}